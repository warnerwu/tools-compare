## tools-compare 项目说明

tools-compare 项目用于快速对比两个数字字符串共同元素及不同元素

### 使用 `composer` 初始化项目目录

> 使用命令

```bash
composer init
```

> 生成 `composer.json` 文件

```json
{
    "name": "warnerwu/computed",
    "description": "快速对比小工具",
    "type": "project",
    "license": "MIT",
    "authors": [
        {
            "name": "warnerwu",
            "email": "warnerwu@126.com"
        }
    ],
    "require" : {
        "silex/silex": "^2.0.4",
        "monolog/monolog": "^1.22",
        "twig/twig": "^2.0",
        "symfony/twig-bridge": "^3"
    },
    "require-dev": {
        "heroku/heroku-buildpack-php": "*"
    },
    "repositories": {
        "packagist": {
            "type": "composer",
            "url": "https://packagist.phpcomposer.com"
        }
    }
}
```


### Heroku 项目部署

#### Heroku 项目初始化

> Heroku 初始化本地项目

```bash
heroku create
```

> 生成如下信息

```bash
➜  contrast heroku create
 ›   Warning: heroku update available from 7.0.41 to 7.0.88
Creating app... done, ⬢ lit-springs-12351
https://lit-springs-12351.herokuapp.com/ | https://git.heroku.com/lit-springs-12351.git
```

#### Heroku 项目部署

```bash
git push -u origin heroku
```

#### Heroku 项目预览

```bash
heroku open
```

#### [项目截图](https://lit-springs-12351.herokuapp.com/)

![项目截图](http://oluvq2vg4.bkt.clouddn.com/image/jpg/tools-compare/tools-compare.png)