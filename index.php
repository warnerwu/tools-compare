<?php
// 设置脚本时区
ini_set("date.timezone", "Asia/Shanghai");

// 比较结果值初始化
$compare_res = null;

// 错误提示信息初始化
$compare_warn_res = null;

// 交集数组初始化
$matches_intersect = null;

// 参数1差集数组【相对于交集的差集】初始化
$params_first_diff = array();

// 参数2差集数组【相对于交集的差集】初始化
$params_second_diff = array();

// 接收输入对比参数1
$params_first = isset($_POST['params-first']) ? $_POST['params-first'] : '';

// 接收输入对比参数2
$params_second = isset($_POST['params-second']) ? $_POST['params-second'] : '';

// 如果当两个对比参数完全不为空时, 才去正则匹配参数数字
if (!empty($params_first) && $params_second) {
    // 匹配整数或浮点数
    preg_match_all('/([0-9]+(\.[0-9]+)?)/', $params_first, $matches_first);
    preg_match_all('/([0-9]+(\.[0-9]+)?)/', $params_second, $matches_second);

    // 对比参数1数组
    $matches_first = $matches_first[0];

    // 对比参数2数组
    $matches_second = $matches_second[0];

    // 两个数组相同的交集
    $matches_intersect = array_intersect($matches_first, $matches_second);

    if ( !empty( $matches_intersect ) ) {
        // 判断参数1中是否包含相对于交集的差集元素, 有则放到 「参数1差集数组」
        if ( count($matches_first) > count($matches_intersect)  ) {
            foreach ( $matches_first as $params_first_item ) {
                if ( !in_array( $params_first_item, $matches_intersect) ) {
                    array_push($params_first_diff, $params_first_item);
                }
            }
        }

        // 判断参数2中是否包含相对于交集的差集元素, 有则放到 「参数1差集数组」
        if ( count($matches_second) > count($matches_intersect)  ) {
            foreach ( $matches_second as $params_second_item ) {
                if ( !in_array( $params_second_item, $matches_intersect) ) {
                    array_push($params_second_diff, $params_second_item);
                }
            }
        }
    } else {
        $compare_warn_res = '对比完成! 不存在交集~对比没有意义';
    }
} else {
    $compare_warn_res = '对比参数不完整！无法完成对比工作~';
}

// 当前时间
$time = date('Y-m-d H:i:s', time());
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="apple-touch-icon" sizes="76x76" href="/resource/images/logo.png">
    <link rel="icon" type="image/png" href="/resource/images/logo.png">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>快速对比</title>
    <style>
        .mg-t-m-1 {
            margin: 1rem auto;
        }

        .text-align-center {
            text-align: center;
        }
    </style>
    <script>
        function clearParams() {
            // 对比参数1表单域对象
            var sParamsFirst = document.querySelector('textarea[name="params-first"]');

            // 对比参数2表单域对象
            var sParamsSecond = document.querySelector('textarea[name="params-second"]');

            // 对比结果
            var aCompareResult = document.querySelectorAll('div[class="card border-info mb-12"]');

            // 循环隐藏对比结果卡片
            aCompareResult.forEach(function (card) {
                card.style.display = 'none';
            });

            // 清空参数1表单域
            sParamsFirst.setAttribute('value', '');
            sParamsFirst.value = '';

            // 清空参数2表单域
            sParamsSecond.setAttribute('value', '');
            sParamsSecond.value = '';
        }
    </script>
</head>
<body>
<!-- Image and text -->
<nav class="navbar navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="resource/images/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
            快速对比
        </a>
    </div>
</nav>
<div class="container">
    <form class="mg-t-m-1" method="post">
        <!-- 请输入参数1 -->
        <div class="form-group">
            <textarea name="params-first" class="form-control" rows="3"
                      placeholder="请输入参数1"><?php if (!empty($params_first)) echo $params_first; ?></textarea>
        </div>

        <!-- 请输入参数2 -->
        <div class="form-group">
            <textarea name="params-second" class="form-control" rows="3"
                      placeholder="请输入参数2"><?php if (!empty($params_second)) echo $params_second; ?></textarea>
        </div>

        <button type="submit" class="btn btn-primary btn-block">对比</button>
        <button type="button" class="btn btn-outline-success btn-block" onclick="clearParams()">清除</button>
    </form>

    <?php
    // 错误提示信息
    if ($compare_warn_res !== null) {
        ?>
        <div class="alert alert-success text-align-center" role="alert">
            <?php
            echo $compare_warn_res;
            ?>
        </div>
        <?php
    }
    ?>

    <?php
    // 交集~比较结果
    if ($matches_intersect) {
        ?>
        <div class="card border-info mb-12">
            <div class="card-header">交集</div>
            <div class="card-body text-info">
                <p class="card-text">
                    <?php
                    echo implode(', ', $matches_intersect);
                    ?>
                </p>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
    // 相对于交集的差集1~比较结果
    if ($params_first_diff) {
        ?>
        <div class="card border-info mb-12">
            <div class="card-header">相对于交集的参数1差集</div>
            <div class="card-body text-info">
                <p class="card-text">
                    <?php
                    echo implode(', ', $params_first_diff);
                    ?>
                </p>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
    // 相对于交集的差集2~比较结果
    if (!empty( $params_second_diff )) {
        ?>
        <div class="card border-info mb-12">
            <div class="card-header">相对于交集的参数2差集</div>
            <div class="card-body text-info">
                <p class="card-text">
                    <?php
                    echo implode(', ', $params_second_diff);
                    ?>
                </p>
            </div>
        </div>
        <?php
    }
    ?>
</div>
</body>
</html>